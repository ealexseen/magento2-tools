const config = {
    main: require('./config/main'),
    theme: require('./config/theme')
};
const gulp = require('gulp');
const sass = require('gulp-sass');
const path = require('path');
const mergeStream = require('merge-stream');
const fs = require('fs');
const browserSync = require('browser-sync').create();

/**
* Css
**/
function initThemes(callback) {
    for(var data in config.theme) {
        callback(data, config.theme);
    }
}

let themePath = [];

initThemes(function(e, theme, i) {
    themePath.push({
        urlSrc: path.resolve(__dirname, '../../..', config.main.srcPathTheme, theme[e].area, theme[e].vendor, e),
        urlDest: path.resolve(__dirname, '../../..', config.main.destPathTheme, theme[e].area, theme[e].vendor, e, theme[e].local),
        arraySrc: function() {
            let array = [];

            array.push(path.join(this.urlSrc, 'web/css/source/*.scss'));
            array.push(path.join(this.urlSrc, '**', 'web/css/source/*.scss'));

            return array;
        },
        nameTheme: e
    });
});

gulp.task('sass', function () {
    let streams = mergeStream();

    themePath.forEach(e => {
        streams.add(
            gulp.src(e.arraySrc())
                .pipe(sass().on('error', sass.logError))
                .pipe(gulp.dest(path.join(e.urlSrc, 'web/css')))
                .pipe(gulp.dest(path.join(e.urlDest, 'css')))
                .pipe(browserSync.reload({stream: true}))
        );
    });

    return streams;
});

gulp.task('watch', () => {
    let cssWatch = themePath.map(e => {
        return e.arraySrc().join(',')
    });

    gulp.watch(cssWatch.toString().split(','), ['sass'])
});

/**
 * browser-sync
 */
gulp.task('browser-sync', () => {
    browserSync.init({
        proxy: {
            target: config.main.localhostUrl
        },
        open: false,
        ghostMode: {
            scroll: false,
            click: false
        }
    });
});

/**
 * default
 */
gulp.task('default', ['browser-sync', 'sass', 'watch']);
